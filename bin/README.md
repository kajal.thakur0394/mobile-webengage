

# Mobile Automation

The Mobile Automation framework is built to test out WebEngage Mobile SDKs. This framework works in conjunction with [Dashboard Automation](https://repo.webengage.com/qa/ui-automation "Dashboard Automation Framework"). Comprises two main suites, Test Data Creation Suite and Campaign Validation Suite.
Tests get triggered only via Dashboard Automation and suite gets chosen depending upon the execution suite that was chosen in upstream Dashboard Automation pipeline.

## Requirements
In order to setup this project you'll need following things installed and configured on your local machine
- Maven
- Java
- Node.js
- Android Studio [Android Emulators]
- Android SDK [adb]
- Xcode
- Carthage
- homebrew
- ideviceinstaller
- iOS-deploy
- Appium Server [node package]
- Appium Doctor [node package]
- Appium Inspector
- Chrome Browser
- Java IDE
    - JSON Editor Plugin
    - Jenkins Editor Plugin

Step by step installation and configuration procedure has been described over [here](https://docs.google.com/document/d/1XsCXl7Q0UFfdFkne6hDEIP6Koo95qmjuv1z0eP68J14/edit "Appium installation").

## Tech Stack

- Appium Server
- Appium Java Client
- Emulator/Simulator Configs
- TestNG
- CICD Pipelines
- JSch
- JSON-simple
- Gson
- Log4j


## Test Suites
There are two Suites in this framework whose selection is dependent on the suite that gets executed in upstream Dashboard Automation job.


### Test Data Creation Suite

This gets triggered when Test Data Creation Suite is executed via Dashboard Automation Pipeline. The Mobile Users which are part of Pre-requisite Test Data get created via this suite. This gets executed for both Android and iOS users on their respective emulators.
It can be triggered explicitly via [build parameters](https://jenkins.stg.webengage.biz/job/API%20Automation/build?delay=0sec "build Job") page. In order to make the Campaign Validation suite function properly it is expected that Users are engaged and this activity can be done by mentioned suite.

### Campaign Validation Suite

Mobile SDK Campaigns which gets launched via upstream Dashboard Automation SDKTesting suite get validated by this Suite. This comprises Tests that validate campaigns across various Mobile channels.
A generic content validation engine has been built and the TC that is to be tested is decided by the campaign payload template it parses. And, the campaign template is used for driving upstream job to launch campaigns targeted at engaged users.
This suite cannot be executed via Jenkins [build parameters](https://jenkins.stg.webengage.biz/job/API%20Automation/build?delay=0sec "build Job") page.
## Capabilities

### Modules
The modules 
- Push Campaigns
- InApp Campaigns

### Environments
The above suites can be executed on any of these environments. The environment specific Packages have been maintained in the project 
- Prod US
- Prod IN
- Staging

### Account & Packages
Depending upon the environment chosen the packages get selected and configured on chosen devices.
To enable users on specific account user can provide a license code as a parameter for the same.

### Devices
Both Android and iOS devices can be plugged into the framework. These devices can either be physical devices or emulator/simulator instances.
In order to direct your tests to a specific device you need to provide device's unique identifier (udid) in [deviceConfig.json](./src/test/resources/deviceConfig.json)

## Usage

In order to make use of this framework you need devices configured and [deviceConfig.json](./src/test/resources/deviceConfig.json) pointing to these devices.
Appium server should be up and running, two server instances need to be running, each instance for a specific platform.

Host Appium server for Android Tests on default port 4723
```bash
  appium -p 4723
```
Host Appium server for Android Tests on default port 4725
```bash
  appium -p 4725
```

Tests have been organized as TestNG test suites. Each Test class is specific to a Mobile channel and the test cases are fed via Data Provider. Hence, the number of tests in SDKTesting of Dashboard automation module-wise translates to the number of tests in Mobile project
To compile the project
```bash
  mvn clean compiler:compile
```
XMLs specific to modules has been devised that act as a parameter for maven builds
```bash
  -Dset.LicenseCode=~10a5cb5c6 -Dset.Environment=ProdUS -DsuiteXmlFile=PushCampaigns_testng.xml test
  -Dset.LicenseCode=~10a5cb5c6 -Dset.Environment=ProdUS -DsuiteXmlFile=InAppCampaigns_testng.xml test
  -Dset.LicenseCode=~10a5cb5c6 -Dset.Environment=ProdUS -DsuiteXmlFile=DataCreation_testng.xml test
```
## Execution

The team has configured CICD pipeline to execute build after upstream job with suite as SDKTesting for Mobile is executed

|    Suite   	| Upstream Suite 	| Module in Upstream Job 	|   Pre-requisite  	|
|:----------:	|:-----------:	|:-----------:	|:-------:	|
| Campaign Validation 	| SDK Testing     	| Push    	| User Engaged & reachable	|
| Campaign Validation 	| SDK Testing     	| InApp    	| User Engaged & reachable 	|
| Test Data Creation 	| Test Data Creation     	| -   	| User Data 	|

User can also trigger Test Data Creation build by passing parameter directly via Jenkins. User needs to provide the environment and license code for the same

[Click here](https://jenkins.stg.webengage.biz/job/Mobile%20Automation/build?delay=0sec "Trigger Mobile Automation") to Trigger the Pipeline


## Reporting

Reporting has been partially implemented where the results in form of Screenshots are exported to a folder in project directory which are then to be imported and embedded into Serenity Reports by Dashboard Automation project.
Meanwhile, for the time being, a slack message with execution details is sent in the same thread where upstream job results are logged.
## Documentation

Refer to the following docs to get more info about the project

[Architecture](https://webengage.atlassian.net/wiki/spaces/QA/pages/2911109121/Mobile+Framework)

[Latest Release Notes](https://repo.webengage.com/qa/mobile-automation/-/releases)

[Releases](https://linktodocumentation)

[Jenkins Pipeline](https://jenkins.stg.webengage.biz/job/Mobile%20Automation/)

