package com.webengage.mobile_automation.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BaseDAO {
	ResultSetMetaData metaData = null;

	public List<HashMap<Object, String>> getResultSetObj(ResultSet rs) throws SQLException {
		List<HashMap<Object, String>> data = new ArrayList<>();
		while (rs.next()) {
			HashMap<Object, String> row = new HashMap<Object, String>();
			row.put("campaign_name",rs.getObject(2).toString());
			row.put("user_id",rs.getObject(4).toString());
			row.put("template",rs.getObject(5).toString());
			data.add(row);
		}
		return data;
	}
}
