package com.webengage.mobile_automation.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.webengage.automation_core.database.*;

public class CampaignDAO {
	Connection con;
	BaseDAO baseD = new BaseDAO();
	private static CampaignDAO instance;

	private CampaignDAO() {
		try {
			con = DatabaseUtility.getConnection("we_mobiledata");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<HashMap<Object, String>> getCampaignData(String module) throws SQLException {
		ResultSet rs = DatabaseUtility.executeQuery(con,
				"select * from campaign_data where feature ='"+module+"' and campaign_name='MobileSDKInAppHeader'");
				//"select * from campaign_data where feature ='"+module+"'");
		List<HashMap<Object, String>> resultSet = baseD.getResultSetObj(rs);
		return resultSet;
	}
	
	public static CampaignDAO getInstance() {
        if (instance == null) {
            instance = new CampaignDAO();
        }
        return instance;
    }
}
