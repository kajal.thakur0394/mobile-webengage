package com.webengage.mobile_automation.dataProviders;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.testng.annotations.DataProvider;
import com.webengage.mobile_automation.dao.CampaignDAO;

public class CampaignPayloadDP {
	List<HashMap<Object, String>> resultSet = new ArrayList<>();
	String module = System.getProperty("set.Module");
	
	@DataProvider(name = "InApp")
	public Iterator<HashMap<Object, String>> inApp() {
		return getTestData("InApp");
	}
	
	@DataProvider(name = "Push")
	public Iterator<HashMap<Object, String>> pushCreds() {
		return getTestData("Push");
	}
	
	@DataProvider(name = "AppInLine")
	public Iterator<HashMap<Object, String>> appInLine() {
		return getTestData("AppInLine");
	}
	
	private Iterator<HashMap<Object, String>> getTestData(String module) {
		try {
			if (this.module.contains(module)) {
				CampaignDAO camp = CampaignDAO.getInstance();
				resultSet = camp.getCampaignData(module);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet.iterator();
	}
}
