package com.webengage.mobile_automation.driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import org.json.simple.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.webengage.mobile_automation.utils.BaseSetup;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidDriverManager implements DriverImplementer {

	public static final String APPPACKAGE_PATH = System.getProperty("user.dir") + "/src/test/resources/appPackages/"
			+ DriverManager.environment + "/android.apk";

	@Override
	public AppiumDriver getDeviceDriver(JSONObject deviceConfig) throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		if (!BaseSetup.isAppPackageReqd) {
			deviceConfig.remove("appPackage");
			deviceConfig.remove("appActivity");
		}
		deviceConfig.keySet().forEach(key -> {
			capabilities.setCapability((String) key, (String) deviceConfig.get(key));
		});
		capabilities.setCapability("noReset", System.getProperty("noReset"));
		capabilities.setCapability("chromedriver_autodownload", true);
		if (!(Boolean.valueOf(System.getProperty("noReset"))))
			capabilities.setCapability("app", APPPACKAGE_PATH);
		return new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
	}
}
