package com.webengage.mobile_automation.driver;

import io.appium.java_client.AppiumDriver;

import java.net.MalformedURLException;

import org.json.simple.JSONObject;

public interface DriverImplementer {

    public AppiumDriver getDeviceDriver(JSONObject obj) throws MalformedURLException;

}
