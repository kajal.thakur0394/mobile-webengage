package com.webengage.mobile_automation.driver;

import io.appium.java_client.AppiumDriver;

import java.net.MalformedURLException;

import org.json.simple.JSONObject;

import com.webengage.mobile_automation.utils.Log;

public class DriverManager {
public static AppiumDriver driver;

public static String environment=System.getProperty("set.Environment");

    public void initializeDriver(JSONObject deviceConfig) throws MalformedURLException {
        String platform=System.getProperty("set.Platform");
        Log.info("Chosen env:"+environment);
        switch(platform){
            case "Android":
                    driver=new AndroidDriverManager().getDeviceDriver((JSONObject)deviceConfig.get("Android"));
                    break;
            case "iOS":
                    driver=new IOSDriverManager().getDeviceDriver((JSONObject)deviceConfig.get("iOS"));
                break;
            default:
                break;
        }
    }
    

}
