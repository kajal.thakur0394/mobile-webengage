package com.webengage.mobile_automation.driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

import org.json.simple.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class IOSDriverManager implements DriverImplementer {

	@Override
	public AppiumDriver getDeviceDriver(JSONObject deviceConfig) throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		deviceConfig.keySet().forEach(key -> {
			try {
				capabilities.setCapability((String) key, (String) deviceConfig.get(key));
			} catch (ClassCastException e) {
				capabilities.setCapability((String) key, (Boolean) deviceConfig.get(key));
			}
		});
		String packageType = ((String) deviceConfig.get("deviceForm")).contains("virtual") ? "/ios_sim.zip"
				: "/ios.ipa";
		capabilities.setCapability("app", System.getProperty("user.dir") + "/src/test/resources/appPackages/"
				+ DriverManager.environment + packageType);
		capabilities.setCapability("noReset", "false");
		//capabilities.setCapability("fullReset", "true");
		return new IOSDriver(new URL("http://localhost:4725/wd/hub"), capabilities);
	}
}
