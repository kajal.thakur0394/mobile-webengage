package com.webengage.mobile_automation.locators;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.webengage.mobile_automation.driver.DriverManager;

public class EngageScreenLocators {

	public EngageScreenLocators() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.driver), this);
	}

	@iOSXCUITFindBy(accessibility = "Clear text")
	public WebElement clearLicenseField;

	@AndroidFindBy(id = "com.webengage.environment:id/licenseEditText")
	@iOSXCUITFindBy(iOSNsPredicate = "value == 'Enter License Code'")
	public WebElement licenseField;

	@AndroidFindBy(id="com.webengage.production:id/in")
	public WebElement indiaEnvironment;
	
	@AndroidFindBy(id = "com.webengage.environment:id/engageButton")
	@iOSXCUITFindBy(iOSNsPredicate = "label == 'Set License ' AND name == 'Set License ' AND value == 'Set License '")
	public WebElement engageButton;

	@AndroidFindBy(id = "com.webengage.environment:id/menu_login")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Login']")
	public WebElement loginButton;

	@AndroidFindBy(id = "com.webengage.environment:id/usernameEditText")
	@iOSXCUITFindBy(iOSNsPredicate = "value == 'Enter Login ID'")
	public WebElement userNameField;

	@AndroidFindBy(id = "android:id/button1")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeButton[@name='Login'])[2]")
	public WebElement setUserBtn;
	
	@iOSXCUITFindBy(accessibility = "Okay")
	public WebElement okayButton;

}
