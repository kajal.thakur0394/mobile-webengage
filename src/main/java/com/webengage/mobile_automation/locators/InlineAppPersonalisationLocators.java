package com.webengage.mobile_automation.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import com.webengage.mobile_automation.driver.DriverManager;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class InlineAppPersonalisationLocators {
    
    public InlineAppPersonalisationLocators() {
        PageFactory.initElements(new AppiumFieldDecorator(DriverManager.driver), this);
    }
    
    @AndroidFindBy(id = "com.webengage.production:id/tv_name")
    public WebElement options;
    
    @AndroidFindBy(id = "com.webengage.production:id/personalizationButton")
    public WebElement inlinePersonalisationButton;
    
    @AndroidFindBy(id = "com.webengage.production:id/btn")
    public WebElement screenButton;
    
    @AndroidFindBy(id = "com.webengage.production:id/S1P1")
    public WebElement screen1Property1;
    
    @AndroidFindBy(id = "com.webengage.production:id/we_banner_title")
    public WebElement bannerTitle;
    
    @AndroidFindBy(id = "com.webengage.production:id/we_banner_description")
    public WebElement bannerDescription;
    
    @AndroidFindBy(id = "com.webengage.production:id/we_banner_btn1")
    public WebElement bannerButton;

    @AndroidFindBy(id = "com.webengage.production:id/S2P1")
    public WebElement screen2Property1;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.TextView")
    public WebElement screenName;

    @AndroidFindBy(id = "com.webengage.production:id/tv_name")
    public WebElement nameKeyText;

    @AndroidFindBy(id = "com.webengage.production:id/tv_email")
    public WebElement emailKeyText;


}
