package com.webengage.mobile_automation.locators;

import org.openqa.selenium.WebElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.lang.reflect.Field;
import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.utils.Log;

public class NotificationPanelLocators {

    public NotificationPanelLocators() {
        PageFactory.initElements(new AppiumFieldDecorator(DriverManager.driver), this);
    }

    @AndroidFindBy(id = "android:id/status_bar_latest_event_content")
    public List<WebElement> NotificationFrame;
    @AndroidFindBy(id = "com.webengage.production:id/push_base_container")
    public List<WebElement> NotificationFrameRc;

    @AndroidFindBy(id = "android:id/app_name_text")
    public WebElement appName;

    @AndroidFindBy(id = "com.webengage.production:id/app_name")
    public WebElement appNameRc;

    @AndroidFindBy(id = "android:id/title")
    public WebElement title;

    @AndroidFindBy(id = "android:id/expand_button")
    public WebElement expand_btn;

    @AndroidFindBy(id = "com.webengage.production:id/custom_title")
    public WebElement titleRc;

    @AndroidFindBy(id = "android:id/big_text")
    public WebElement descBig;

    @AndroidFindBy(id = "android:id/text")
    public WebElement desc;

    @AndroidFindBy(id = "com.webengage.production:id/custom_message")
    public WebElement descRc;

    @AndroidFindBy(id = "android:id/expand_button")
    public WebElement expand;

    @AndroidFindBy(id = "com.webengage.production:id/custom_summary")
    public WebElement msgSummaryCarousel;

    @AndroidFindBy(id = "android:id/header_text")
    public WebElement msgSummary;

    @AndroidFindBy(id = "android:id/action0")
    public WebElement advOptionLabel;

    @AndroidFindBy(id = "com.webengage.production:id/rating_v1_title")
    public WebElement ratingSectionTitle;

    @AndroidFindBy(id = "com.webengage.production:id/rating_v1_message")
    public WebElement ratingSectionDesc;

    @AndroidFindBy(id = "com.webengage.production:id/carousel_landscape_desc")
    public WebElement imgBtnLabelCarousel;

    @AndroidFindBy(id = "com.webengage.production:id/next")
    public WebElement nextBtnCarousel;
    
    @AndroidFindBy(accessibility="Collapse")
    public WebElement collapse;

    public String getByLocator(String fieldName) {
        Field f = null;
        try {
            f = NotificationPanelLocators.class.getField(fieldName);
        } catch (NoSuchFieldException | SecurityException e) {
            Log.error("Specified field " + fieldName + " is not present");
        }
        return f.getDeclaredAnnotation(AndroidFindBy.class).id();
    }

}
