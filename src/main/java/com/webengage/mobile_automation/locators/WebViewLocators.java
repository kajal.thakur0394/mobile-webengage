package com.webengage.mobile_automation.locators;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.webengage.mobile_automation.driver.DriverManager;

public class WebViewLocators {

	public WebViewLocators() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.driver), this);
	}

	@FindBy(xpath = "//div[@class='description']")
	public WebElement description;
	
	@FindBy(xpath = "//img[contains(@class,'img')]")
	public WebElement icon;
	
	@FindBy(xpath = "//button[text()='Close']")
	public WebElement CLOSE_BTN;
	
	@FindBy(xpath = "//div[@class='n-preview-title']/div")
    public WebElement popoutLayoutTitle;
	
	@FindBy(xpath = "//div[@class='n-preview-subtitle']/div")
    public WebElement popoutLayoutDescription;
	
	@FindBy(xpath = "//div[contains(@class,'button')]/div")
    public WebElement popoutLayoutButton;
	
	@FindBy(xpath = "//button[@class='button']")
    public WebElement primaryButton;
	
	@FindBy(xpath ="//button[@class='button close']")
	  public WebElement closeButtonHeaderLayout;
}
