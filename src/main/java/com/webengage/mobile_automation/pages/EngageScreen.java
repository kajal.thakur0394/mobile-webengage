package com.webengage.mobile_automation.pages;

import java.util.concurrent.TimeUnit;

import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.locators.EngageScreenLocators;
import com.webengage.mobile_automation.utils.BaseSetup;
import com.webengage.mobile_automation.utils.Log;
import io.appium.java_client.ios.IOSDriver;

public class EngageScreen extends BaseSetup {
	String env = DriverManager.environment.contains("Prod") || DriverManager.environment.contains("QA") ? "production"
			: "staging";
	EngageScreenLocators engage = new EngageScreenLocators();

	public void androidSetup() {
		sendKeys(dynamicLocatorReplaceText(engage.licenseField, "environment", env),
				System.getProperty("set.LicenseCode"));
		if (DriverManager.environment.trim().equalsIgnoreCase("ProdIN")) {
			Log.info("Selecting IN env from Android Prod SDK build");
			click(engage.indiaEnvironment);
		}
		click(dynamicLocatorReplaceText(engage.engageButton, "environment", env));
	}

	public void userLogin(String userId) {
		click(dynamicLocatorReplaceText(engage.loginButton, "environment", env));
		sendKeys(dynamicLocatorReplaceText(engage.userNameField, "environment", env), userId);
		click(engage.setUserBtn);
	}

	public void iOSLogin() {
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//DriverManager.driver.switchTo().alert().accept();
		click(engage.clearLicenseField);
		sendKeys(engage.licenseField, System.getProperty("set.LicenseCode"));
		click(engage.engageButton);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String bundleName = null;
		switch (DriverManager.environment) {
		case "ProdUS":
		case "Prod US":
			bundleName = "com.webEngage.prodSwift";
			break;
		case "ProdIN":
			bundleName = "com.webEngage.prodSwift";
			break;
		case "QA":
			bundleName = "com.webEngage.prodSwift";
			break;
		case "Staging":
			bundleName = "com.webEngage.stageSwift";
			break;
		default:
			break;
		}
		((IOSDriver) DriverManager.driver).activateApp(bundleName);
		click(engage.loginButton);
		sendKeys(engage.userNameField, "TestUser001");
		click(engage.setUserBtn);
		click(engage.okayButton);
	}
}