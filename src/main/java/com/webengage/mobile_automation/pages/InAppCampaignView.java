package com.webengage.mobile_automation.pages;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import org.json.simple.JSONObject;
import org.testng.Assert;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.locators.WebViewLocators;
import com.webengage.mobile_automation.utils.BaseSetup;
import com.webengage.mobile_automation.utils.CustomAssertions;
import com.webengage.mobile_automation.utils.Log;


import io.appium.java_client.remote.SupportsContextSwitching;


public class InAppCampaignView extends BaseSetup {

    WebViewLocators webViewLocator = new WebViewLocators();
    String webViewName;

    public String getWebViewName() {
        return webViewName;
    }

    public void setWebViewName(String webViewName) {
        this.webViewName = webViewName;
    }

    public void waitForWebView() {
        if ((System.currentTimeMillis() - getLaunchTimeInstance()) < 180000) {
            Set<String> contextNames = ((SupportsContextSwitching)DriverManager.driver).getContextHandles().stream()
                    .filter(s -> s.contains("webengage")).collect(Collectors.toSet());
            if (contextNames.size() == 0) {
                waitForWebView();
                return;
            }
            for (String contextName : contextNames) {
                double renderTime = (System.currentTimeMillis() - getLaunchTimeInstance()) / 1000;
                Log.info("Time taken by InApp Campaign to render was " + renderTime + " seconds");
                ((SupportsContextSwitching)DriverManager.driver).context(contextName);
                setWebViewName(contextName);
            }
        } else {
            Log.error("InApp Campaign was not rendered. Timeout Duration has been set to 2 minutes");
        }
    }
    
	public void validateContents(JSONObject json) {
		((SupportsContextSwitching) DriverManager.driver).context(getWebViewName());
		JSONObject expectedContent = (JSONObject) json.get("Content");
		expectedContent.keySet().forEach(currentKey -> {
			switch ((String) currentKey) {
			case "MessageCard":
				JSONObject expectedMessage = (JSONObject) expectedContent.get("MessageCard");
				validateMessageContents(expectedMessage);
				break;
			case "ButtonsCardHeaderLayout":
				JSONObject expectedButtons = (JSONObject) expectedContent.get("ButtonsCardHeaderLayout");
				validateButtonsContent(expectedButtons);
				break;
			case "ButtonCardPopoutModalLayout": {
				JSONObject expectedButton = (JSONObject) expectedContent.get("ButtonCardPopoutModalLayout");
				CustomAssertions.assertEquals("Content not matched->", expectedButton.get("PopoutButtonLabel"),
						getText(webViewLocator.popoutLayoutButton));
				break;
			}
			default:
				CustomAssertions.assertTrue("Content not found in provided JSON", false);
				break;
			}
		});
	}

    private void validateButtonsContent(JSONObject expectedButtons) {
        expectedButtons.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s) {
                case "PrimaryButtonLabel":
                    actualValue = getText(webViewLocator.primaryButton);
                    break;
                case "CloseButtonLabel":
                    actualValue = getText(webViewLocator.closeButtonHeaderLayout);
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
            CustomAssertions.assertEquals("Content not matched->",expectedButtons.get((String) s), actualValue);
        });

    }

    private void validateMessageContents(JSONObject expectedMessageCardContents) {
        expectedMessageCardContents.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s) {
                case "HeaderLayoutDescription":
                    actualValue = getText(webViewLocator.description);
                    break;
                case "PopoutModalLayoutDescription":
                    actualValue = getText(webViewLocator.popoutLayoutDescription);
                    break;
                case "Icon":
                    actualValue = getAttribute(webViewLocator.icon, "src");
                    break;
                case "Title":
                    actualValue = getText(webViewLocator.popoutLayoutTitle);
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
            CustomAssertions.assertEquals("Content not matched->", expectedMessageCardContents.get((String) s), actualValue);
        });

    }
   
}
