package com.webengage.mobile_automation.pages;

import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.locators.InlineAppPersonalisationLocators;
import com.webengage.mobile_automation.utils.BaseSetup;
import com.webengage.mobile_automation.utils.CustomAssertions;
import com.webengage.mobile_automation.utils.Log;
import io.appium.java_client.android.AndroidDriver;

public class InlineAppPersonalisationPage extends BaseSetup {
    double launchTimeInstance;
    InlineAppPersonalisationLocators inlineAppLocators = new InlineAppPersonalisationLocators();

    public void openLandingPage(JSONObject json) throws IOException{
        String campaignType = (String) json.get("Type");
        switch (campaignType){
            case "Banner View":
            case "Text View":
                openPropertyListPage();
                break;
            case "Custom View":
                openScreenPage();
                break;
            default:
                Log.info("Please check if Campaign Type in Expected is correct");
                break;
        }
    }
    public void openPropertyListPage() throws IOException {
        openScreenPage();
        click(inlineAppLocators.screenButton);
        Log.info("User successfully reached at propertyID page.");
        exportScreenshot(false, null, "AppInLine Content Page");
    }

    public void openScreenPage(){
        //click(inlineAppLocators.options);
        click(inlineAppLocators.inlinePersonalisationButton);
        Log.info("User successfully reached at the screen page.");
    }

    public void validateContents(JSONObject json) throws IOException{
    	JSONArray expectedContent = (JSONArray) json.get("Content");
        Iterator<?> objectIterator = expectedContent.iterator();
        while(objectIterator.hasNext()){
            JSONObject jsonObject = (JSONObject) objectIterator.next();
            Iterator<String> keys = jsonObject.keySet().iterator();
            while (keys.hasNext()) {
                String currentKey = keys.next();
                switch (currentKey) {
                    case "TextAndAction":
                        JSONObject expectedTextAndActionContent = (JSONObject) jsonObject.get("TextAndAction");
                        validateTextAndActionContents(expectedTextAndActionContent);
                        break;
                    case "Button":
                        JSONObject expectedButtonContent = (JSONObject) jsonObject.get("Button");
                        validateButtonContent(expectedButtonContent);
                        break;
                    case "OnClickAction":
                        JSONObject expectedOnClickActionContent = (JSONObject) jsonObject.get("OnClickAction");
                        validateOnClickAction(expectedOnClickActionContent);
                        break;
                    case "KeyValue":
                        JSONObject expectedKeyValueContent = (JSONObject) jsonObject.get("KeyValue");
                        validateKeyValueContent(expectedKeyValueContent);
                    default:
                        Log.debug("Please check if the keyword in expected content is correct");
                        break;
                }
            }
        }
    }

    private void validateTextAndActionContents(JSONObject expectedTextAndActionContent) {
        expectedTextAndActionContent.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s) {
                case "Title":
                    actualValue = getText(inlineAppLocators.bannerTitle);
                    break;
                case "Description":
                    actualValue = getText(inlineAppLocators.bannerDescription);
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
           CustomAssertions.assertEquals("Value didn't matched ",expectedTextAndActionContent.get((String) s), actualValue);         
        });

    }

    private void validateButtonContent(JSONObject expectedButtonContent) {
        expectedButtonContent.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s) {
                case "ButtonLabel":
                    actualValue = getText(inlineAppLocators.bannerButton);
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
            CustomAssertions.assertEquals("Button label didn't matched ",expectedButtonContent.get((String) s), actualValue);
        });

    }

    private void validateOnClickAction(JSONObject expectedOnClickActionContent){
        click(inlineAppLocators.bannerButton);
        expectedOnClickActionContent.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s) {
                case "screenName":
                    actualValue = getText(inlineAppLocators.screenName);
                    Assert.assertTrue(inlineAppLocators.screen2Property1.isDisplayed());
                    break;
                case "packageName":
                    switchToWebContext();
                    actualValue = ((AndroidDriver) DriverManager.driver).getCurrentPackage();
                    break;
            }
            CustomAssertions.assertEquals("Value didn't matched ",expectedOnClickActionContent.get((String) s), actualValue);

        });
    }

    private void validateKeyValueContent(JSONObject expectedKeyValueContent){
        expectedKeyValueContent.keySet().forEach(s -> {
            String actualValue = null;
            switch ((String) s){
                case "Name":
                    actualValue = getText(inlineAppLocators.nameKeyText);
                    break;
                case "Email":
                    actualValue = getText(inlineAppLocators.emailKeyText);
                    break;
            }
            CustomAssertions.assertEquals("Value didn't matched ",expectedKeyValueContent.get((String) s),actualValue);
        });

    }
}
