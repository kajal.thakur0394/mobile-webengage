package com.webengage.mobile_automation.pages;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONObject;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.locators.NotificationPanelLocators;
import com.webengage.mobile_automation.utils.BaseSetup;
import com.webengage.mobile_automation.utils.CustomAssertions;
import com.webengage.mobile_automation.utils.Log;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class PushNotificationsPanel extends BaseSetup {

    NotificationPanelLocators notif = new NotificationPanelLocators();

    public void openNotificationPanel() {
        ((AndroidDriver) DriverManager.driver).openNotifications();
        setLaunchTimeInstance(System.currentTimeMillis());
        

    }

	public void filterAppNotifications(JSONObject json) throws IOException, InterruptedException {
		boolean isTitleFound = false;
		List<WebElement> list=null;
		outer:do {
			list=notif.NotificationFrame;
			if(list.isEmpty())
				continue;
			for (int i = 0; i < list.size(); i++) {
				WebElement webElement = list.get(i);
				String campaignTitle = (String) json.get("Title");
				Log.debug("Current JSONTemplate title: " + campaignTitle);
				try {
					if (!checkTitlePresent(webElement, "title")) {
						Log.debug("Current title in the notification DOM: "
								+ getText(webElement, notif.getByLocator("title")));
						if (campaignTitle.equals(getText(webElement, notif.getByLocator("title")))) {
							exportScreenshot(true, webElement, (String) json.get("Name") + "InAppNotif");
							isTitleFound = validateFromLayout((String) json.get("Type"), webElement, json);
							if (isTitleFound) {
								break outer;
							}
						}
					}
				} catch (NoSuchElementException e) {
					Log.debug("inside filterAppNotification's catch");
					e.printStackTrace();
				}
			}
		} while ((System.currentTimeMillis() - getLaunchTimeInstance()) < 120000);
		CustomAssertions.assertEquals("Push notif not found",isTitleFound, true);
	}

	private boolean validateFromLayout(String campaignLayoutType, WebElement webElement, JSONObject json) {
		boolean isTitleFound = false;
		JSONObject expectedContent = (JSONObject) json.get("Content");
		JSONObject expectedTextAndAction = (JSONObject) expectedContent.get("TextAndAction");
		JSONObject expectedAdvancedOption = (JSONObject) expectedContent.get("AdvancedOption");
		try {
			String appName = getText(webElement, notif.getByLocator("appName"));
			Log.debug("Current App Name: " + appName);
			if (appName.contains("Prod") || appName.contains("Staging")) {
				// click(webElement, notif.getByLocator("expand_btn"));
				List<WebElement> webEl = notif.NotificationFrame;
				switch (campaignLayoutType) {
				case "Text":
					isTitleFound = validateTextAndActionContents(webEl.get(0), "title", "descBig",
							expectedTextAndAction, isTitleFound);
					validateAdvancedOptionContents(webEl.get(0), "msgSummary", "advOptionLabel",
							expectedAdvancedOption);
					webEl.get(0).click();
					((AndroidDriver) DriverManager.driver).pressKey(new KeyEvent(AndroidKey.HOME));
					break;
				case "Banner":
					isTitleFound = validateTextAndActionContents(webEl.get(0), "title", "desc", expectedTextAndAction,
							isTitleFound);
					validateAdvancedOptionContents(webEl.get(0), "msgSummary", "advOptionLabel",
							expectedAdvancedOption);
					webEl.get(0).click();
					((AndroidDriver) DriverManager.driver).pressKey(new KeyEvent(AndroidKey.HOME));
					break;

				case "Carousel":
					JSONObject expectedCarouselSection = (JSONObject) expectedContent.get("CarouselSection");
					webEl = notif.NotificationFrameRc;
					if (getValidAppName(webEl.get(0), "appNameRc")) {
						isTitleFound = validateTextAndActionContents(webEl.get(0), "titleRc", "descRc",
								expectedTextAndAction, isTitleFound);
						validateAdvancedOptionContents(webEl.get(0), "msgSummaryCarousel", null,
								expectedAdvancedOption);
						validateCarouselLayout(webEl.get(0), "imgBtnLabelCarousel", "nextBtnCarousel",
								expectedCarouselSection);
						webEl.get(0).click();
						((AndroidDriver) DriverManager.driver).pressKey(new KeyEvent(AndroidKey.HOME));
						break;
					}
					break;
				case "Rating":
					JSONObject expectedRatingSection = (JSONObject) expectedContent.get("RatingSection");
					webEl = notif.NotificationFrameRc;
					if (getValidAppName(webEl.get(0), "appNameRc")) {
						isTitleFound = validateTextAndActionContents(webEl.get(0), "titleRc", "descRc",
								expectedTextAndAction, isTitleFound);
						validateRatingLayout(webEl.get(0), "ratingSectionTitle", "ratingSectionDesc",
								expectedRatingSection);
						webEl.get(0).click();
						((AndroidDriver) DriverManager.driver).pressKey(new KeyEvent(AndroidKey.HOME));
						break;
					}

					break;
				default:
					Log.error("Please check if the keyword in expected content is correct");
					break;
				}
			}
		} catch (Exception e) {
			Log.info("inside validateFromLayout catch");
			e.printStackTrace();
		}
		return isTitleFound;
	}

    private void validateRatingLayout(WebElement webEl, String ratingTitleLocator, String ratingDescLocator,
            JSONObject expectedContent) {
        Iterator<String> keys = expectedContent.keySet().iterator();
        while (keys.hasNext()) {
            String currentKey = keys.next();
            switch (currentKey) {
                case "RatingTitle":
                    String actualRatingTitle = getText(webEl, notif.getByLocator(ratingTitleLocator));
                    Assert.assertEquals((String) expectedContent.get(currentKey), actualRatingTitle);
                    break;
                case "RatingDescription":
                    String actualRatingDescription = getText(webEl, notif.getByLocator(ratingDescLocator));
                    Assert.assertEquals((String) expectedContent.get(currentKey), actualRatingDescription);
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
        }

    }

    private void validateCarouselLayout(WebElement webEl, String imgLabelLocator, String nextBtnLocator,
            JSONObject expectedContent) {
        String expectedLabel1 = (String) expectedContent.get("ImageButtonLabel1");
        String expectedLabel2 = (String) expectedContent.get("ImageButtonLabel2");
        Assert.assertEquals(expectedLabel1, getText(webEl, notif.getByLocator(imgLabelLocator)));
        click(notif.nextBtnCarousel);
        Assert.assertEquals(expectedLabel2, getText(webEl, notif.getByLocator(imgLabelLocator)));

    }

    private void validateAdvancedOptionContents(WebElement webElement, String msgSummaryLocator,
            String btnLabelLocator, JSONObject expectedContent) {
        Iterator<String> keys = expectedContent.keySet().iterator();
        while (keys.hasNext()) {
            String currentKey = keys.next();
            switch (currentKey) {
                case "MessageSummary":
                    String actualMsgSummary = getText(webElement, notif.getByLocator(msgSummaryLocator));
                   CustomAssertions.assertEquals("Message Summary not  matched-> ",(String) expectedContent.get(currentKey), actualMsgSummary);
                    break;
                case "AdvOptionBtnLabel":
                    String actualButtonLabel = getText(webElement, notif.getByLocator(btnLabelLocator));
                    CustomAssertions.assertEquals("Adv Option button not label  matched-> ",(String) expectedContent.get(currentKey), actualButtonLabel);
                    break;
                default:
                    Log.error("Please check if the keyword in expected content is correct");
                    break;
            }
        }
    }

    private boolean getValidAppName(WebElement webEl, String locatorString) {
        boolean isAppNamePresent = false;
        try {
            isAppNamePresent = getText(webEl, notif.getByLocator(locatorString)).contains("Production")
                    || getText(webEl, notif.getByLocator(locatorString)).contains("Staging");
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return isAppNamePresent;

    }

    private boolean checkTitlePresent(WebElement webEl, String locatorString) {
        boolean istitlePresent = false;
        try {
            istitlePresent = getText(webEl, notif.getByLocator("title")).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return istitlePresent;

    }

    private boolean validateTextAndActionContents(WebElement webElement, String titleLocatorName,
            String descLocatorName, JSONObject expectedContent, boolean isTitleFound) {
        Iterator<String> keys = expectedContent.keySet().iterator();
        while (keys.hasNext()) {
            String currentKey = keys.next();
            switch (currentKey) {
                case "Title":
                    isTitleFound = validateTextAndActionTitle(webElement, titleLocatorName, expectedContent,
                            isTitleFound);
                    break;
                case "Description":
                    validateTextAndActionDescription(webElement, descLocatorName,
                            (String) expectedContent.get(currentKey));
                    break;
                default:
                    Log.debug("Please check if the keyword in expected content is correct");
                    break;
            }
        }
        return isTitleFound;

    }

    private void validateTextAndActionDescription(WebElement webElement, String descLocatorName,
            String expectedDescription) {
        String actualDescription = getText(webElement, notif.getByLocator(descLocatorName));
        CustomAssertions.assertEquals("Description doesnt matched->",expectedDescription, actualDescription);
    }

    private boolean validateTextAndActionTitle(WebElement webElement, String titleLocatorName,
            JSONObject expectedContent, boolean isTitleFound) {
        String title = getText(webElement, notif.getByLocator(titleLocatorName));
        if (title.equals((String) expectedContent.get("Title"))) {
            isTitleFound = true;
            return isTitleFound;
        }
        return isTitleFound;
    }
}