package com.webengage.mobile_automation.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.remote.Response;

import com.webengage.automation_core.loggers.Log;
import com.webengage.automation_core.apiCoreSetup.RuntimeUtility;
import com.webengage.automation_core.apiCoreSetup.CustomLogFilter;
import com.webengage.automation_core.excelReader.ExcelReader;
import com.webengage.automation_core.apiCoreSetup.APIUtility;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;

public class ApiOperationUtility {
	SetupUtility setupUtility = new SetupUtility();
	Response response;
	static LinkedHashMap<String, String> runtimeURL = new LinkedHashMap<String, String>();
	static LinkedHashMap<String, String> runtimeBody = new LinkedHashMap<String, String>();
	List<String> listName = new ArrayList<>();
	ExcelReader excelReader = new ExcelReader();
	final String excludeListFile = SetupUtility.directoryLoc + "/src/test/resources/testData/excludeFromDelete.json";
	public static String directoryLoc = System.getProperty("user.dir") + "/src/test/resources/testData/runtimeFiles/";
	public static final String directoryLoc1 = System.getProperty("user.dir") + "/src/test/resources/testData/";
	public static final String runtimePayloadsDirectory = directoryLoc1 + "runtimeFiles/";
	RuntimeUtility runtimeUtility = new RuntimeUtility();
	APIUtility apiUtility = new APIUtility();
	CustomLogFilter custLog = new CustomLogFilter();


	public APIUtility generic_post_method(String apiName, String queryParams, String body) throws IOException {
		setupUtility.setApiURI(apiName);
		APIUtility.setJsonBody(body);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		Log.error("JsonBody->"+APIUtility.getJsonBody());
		apiUtility.postRequest(APIUtility.getJsonBody(), queryParameters, apiURI);
		return apiUtility;
	}

	public APIUtility generic_post_method(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.postRequest(apiURI);
		return apiUtility;
	}

	public APIUtility generic_post_method_form_data(String apiName, String queryParams,
			LinkedHashMap<String, String> formData) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.postRequestFormData(formData, queryParameters, apiURI);
		return apiUtility;
	}

	public APIUtility generic_put_method(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.putRequest(apiURI);
		return apiUtility;
	}

	public APIUtility getMethod(String apiURI) throws IOException {
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.getRequest(apiURI);
		return apiUtility;
	}

	public APIUtility generic_put_method(String apiName, String queryParams, String body) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(body));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.putRequest(APIUtility.getJsonBody(), queryParameters, apiURI);
		return apiUtility;
	}
	
	public APIUtility dynamic_post_method(String apiName, String sheetName, String refId) throws IOException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		APIUtility.setJsonBody(apiParams.get("Body"));
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = runtimeUtility.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.postRequest(APIUtility.getJsonBody(), queryParams, apiURI);
		return apiUtility;
	}
	
}
