package com.webengage.mobile_automation.utils;

import com.webengage.mobile_automation.driver.DriverManager;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.SupportsContextSwitching;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;

public class BaseSetup {
    public double launchTimeInstance;
    public AppiumDriver driver;
	public double getLaunchTimeInstance() {
        return launchTimeInstance;
    }

    public void setLaunchTimeInstance(double launchTimeInstance) {
        this.launchTimeInstance = launchTimeInstance;
    }

	JavascriptExecutor js;
	public static String resourcesDirectory = System.getProperty("user.dir") + "/src/test/resources/";
	public static String runtimeTemplatesDirectory = System.getProperty("user.dir")
			+ "/src/test/resources/runtimeTemplates/";
	public static WebDriverWait wait;
	public static boolean isAppPackageReqd = false;

	public BaseSetup() {
		try {
			if (DriverManager.driver == null) {
				new DriverManager().initializeDriver(loadDeviceConfiguration());
			}

		} catch (MalformedURLException | FileNotFoundException e) {
			e.printStackTrace();
		}
		driver = DriverManager.driver;
		js = (JavascriptExecutor) driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(15));
	}

	private JSONObject loadDeviceConfiguration() throws FileNotFoundException {
		String fileLocation = resourcesDirectory + "deviceConfig.json";
		return parseToJSONObject(new FileReader(fileLocation));
	}

	public JSONObject parseToJSONObject(FileReader file) {
		Object templateObj = null;
		try {
			templateObj = new JSONParser().parse(file);
		} catch (Exception e) {
			Log.warn("Exception - " + e);
		}
		JSONObject jsonTemplateObj = (JSONObject) templateObj;
		return jsonTemplateObj;
	}

	public static void flushDirectoryContents() {
		Log.info("Deleting stuff from Runtime Payloads");
		try {
			FileUtils.cleanDirectory(new File(runtimeTemplatesDirectory));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void click(WebElement el) {
		wait.until(ExpectedConditions.elementToBeClickable(el));
		el.click();
	}

	public WebElement findElement(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		WebElement element = driver.findElement(by);
		return  element;
	}
	
	public boolean checkIfElementisLocated(WebElement ele) {
		return ele.isDisplayed();
	}

	public void click(WebElement parentElement, String idLocator) {
		wait.until(ExpectedConditions.elementToBeClickable(parentElement));
		try {
			parentElement.findElement(By.id(idLocator)).click();
		} catch (Exception ElementClickInterceptedException) {
			WebElement element = driver.findElement(By.id(idLocator));
			js.executeScript("arguments[0].click();", element);
		}
	}

	public void sendKeys(WebElement el, String text) {
		wait.until(ExpectedConditions.visibilityOf(el));
		el.sendKeys(text);
	}

	public WebElement dynamicLocatorReplaceText(WebElement el, String textToReplace, String replacingKeyword) {
		String locator = el.toString().replace("Located by By.chained({", "").replace("})", "");
		String locatorType = locator.split(" ")[0].replace("By.", "").replace(":", "");
		String value = locator.split(" ")[1].replace(textToReplace, replacingKeyword);
		switch (locatorType) {
		case "Appiumid":
			By elementBy = By.id(value);
			wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
			return driver.findElement(elementBy);
		default:
			break;
		}
		return el;
	}

	public String getText(WebElement el) {
		wait.until(ExpectedConditions.visibilityOf(el));
		return el.getText();
	}

	public String getAttribute(WebElement el, String attribute) {
		wait.until(ExpectedConditions.visibilityOf(el));
		return el.getAttribute(attribute);
	}

	public String getText(WebElement parentElement, String idLocator) {
		AppiumBy locator = (AppiumBy) AppiumBy.id(idLocator);		
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return parentElement.findElement(locator).getText();
	}
	
	public void exportScreenshot(boolean elementScreenshot, WebElement el, String campIdentifier) throws IOException {
		((SupportsContextSwitching) DriverManager.driver).context("NATIVE_APP");
		File screenshot = DriverManager.driver.getScreenshotAs(OutputType.FILE);
		if (elementScreenshot) {
			try {
				BufferedImage fullScreenshot = ImageIO.read(screenshot);
				Point elPoint = el.getLocation();
				int elHeight = el.getSize().getHeight();
				int elWidth = el.getSize().getWidth();
				BufferedImage elScreenshot = fullScreenshot.getSubimage(elPoint.getX(), elPoint.getY(), elWidth,
						elHeight);
				ImageIO.write(elScreenshot, "png", screenshot);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File screenshotPath = new File(
				System.getProperty("user.dir") + "/src/test/resources/screenshots/" + campIdentifier + ".png");
		try {
			FileUtils.copyFile(screenshot, screenshotPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] fileBytes = new byte[(int) screenshot.length()];
		FileInputStream fileInputStream = new FileInputStream(screenshot);
		fileInputStream.read(fileBytes);
		fileInputStream.close();
		// Encode the byte array as Base64
		String path = Base64.getEncoder().encodeToString(fileBytes);
		ExtentReportUtil.extentTest.addScreenCaptureFromBase64String(path);
	}
	
	  public void killAndLaunchApp() {
	        ((AndroidDriver) DriverManager.driver).terminateApp(getPackageName());
	        ((AndroidDriver) DriverManager.driver).activateApp(getPackageName());
	        setLaunchTimeInstance(System.currentTimeMillis());
	        Log.info("App Instance has been launched for Campaign Verification");
	    }
	public void switchToWebContext() {
		ArrayList<String> contexts = new ArrayList(((SupportsContextSwitching) driver).getContextHandles());
		for (String context : contexts) {
			if (context.contains("WEBVIEW")) {
				((SupportsContextSwitching) driver).context(context);
				Log.info("Switched to webview context with name : " + context);
			}
		}
	}

	public void switchToNativeContext() {
		ArrayList<String> contexts = new ArrayList(((SupportsContextSwitching) driver).getContextHandles());
		for (String context : contexts) {
			if (context.contains("NATIVE_APP")) {
				((SupportsContextSwitching) driver).context(context);
				Log.info("Switched to native context : " + context);
			}
		}
	}
	
	public String getPackageName() {
		return System.getProperty("set.Environment").contains("Prod") ? "com.webengage.production"
				: "com.webengage.staging";
	}
}
