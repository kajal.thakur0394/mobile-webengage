package com.webengage.mobile_automation.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import java.time.LocalTime;

public class ExtentReportUtil {
	public static ExtentReports extentReport;
	public static ExtentTest extentTest;

	public static void initializeExtentReports() throws IOException {
		if (extentReport == null) {
			extentReport = new ExtentReports();
			ExtentSparkReporter sparkReporter = new ExtentSparkReporter(
					System.getProperty("user.dir") + "/test-output/ExtentReport.html");
			extentReport.attachReporter(sparkReporter);
			sparkReporter.config().setDocumentTitle("Mobile Automation");
			LocalTime currentTime = LocalTime.now();
			if (currentTime.isAfter(LocalTime.of(18, 0)) || currentTime.isBefore(LocalTime.of(8, 0))) {
				sparkReporter.config().setTheme(Theme.DARK);
			} else {
				sparkReporter.config().setTheme(Theme.STANDARD);
			}
			sparkReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
		}
	}

	public static void generateExtentReports() throws IOException {
		extentReport.flush();
	}

	public static void showExtentReportsonDesktop() throws IOException {
		Desktop.getDesktop()
				.browse(new File(System.getProperty("user.dir") + "/test-output/ExtentReport.html").toURI());
	}

	public static void createTest(String campaignName) {
		String env = System.getProperty("set.Environment");
		String lc = System.getProperty("set.LicenseCode");
		String platform = System.getProperty("set.Platform");
		extentTest = extentReport.createTest(campaignName, "<b>Environment:</b> " + env
				+ " <br /> <b>License Code:</b> " + lc + " <br /> <b>Platform:</b> " + platform);
	}

	public static void createNode(String nodeName) {
		extentTest = extentTest.createNode(nodeName);
	}
}