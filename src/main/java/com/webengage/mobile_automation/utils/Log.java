package com.webengage.mobile_automation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Log {

	// Initialize Log4j instance
	private static final Logger log = LogManager.getLogger(Log.class);

	// Info Level Logs
	public static void info(String message) {
		ThreadContext.push("Build#"+System.getProperty("set.BuildNumber"));
		log.info(message);
		ThreadContext.clearAll();
	}

	// Warn Level Logs
	public static void warn(String message) {
		log.warn(message);
	}

	// Error Level Logs
	public static void error(String message) {
		ThreadContext.push("Build#"+System.getProperty("set.BuildNumber"));
		log.error(message);
		if(ExtentReportUtil.extentTest!=null) {
			ExtentReportUtil.extentTest.info(message);
		}
		ThreadContext.clearAll();
	}

	// Fatal Level Logs
	public static void fatal(String message) {
		log.fatal(message);
	}

	// Debug Level Logs
	public static void debug(String message) {
		log.debug(message);
	}

}
