package com.webengage.mobile_automation.utils;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

public class RemoteConnection {
	private static String REMOTE_HOST = "10.30.2.141";
	private static int REMOTE_PORT = 22;
	private static String USERNAME = "sftpuser";
	private static String PASSWORD = "usersftp@123";
	private static final int SESSION_TIMEOUT = 10000;
	private static final int CHANNEL_TIMEOUT = 5000;

	public static ChannelSftp setupJsch() throws JSchException {
		JSch jsch = new JSch();
		jsch.setKnownHosts(BaseSetup.resourcesDirectory + "known_hosts");
		Session jschSession = jsch.getSession(USERNAME, REMOTE_HOST, REMOTE_PORT);
		jschSession.setPassword(PASSWORD);
		jschSession.connect(SESSION_TIMEOUT);
		return (ChannelSftp) jschSession.openChannel("sftp");
	}

	public static void readFilesfromRemote(String remoteDir, String localDir) {
		ChannelSftp channelSftp = null;
		try {
			channelSftp = setupJsch();
			channelSftp.connect(CHANNEL_TIMEOUT);
		} catch (JSchException e) {
			e.printStackTrace();
		}
		copyFilefromRemotetoLocal(channelSftp, remoteDir, localDir);
		channelSftp.exit();
	}

	private static void copyFilefromRemotetoLocal(ChannelSftp channelSftp, String remoteDir, String localDir) {
		Collection<ChannelSftp.LsEntry> fileAndFolderList = null;
		try {
			fileAndFolderList = channelSftp.ls(remoteDir);
			fileAndFolderList.forEach(s -> {
				try {
					channelSftp.get(remoteDir + "/" + s.getFilename(),
							BaseSetup.resourcesDirectory + localDir + "/" + s.getFilename());
					Log.info("Copied file " + s.getFilename());
				} catch (SftpException e) {
					e.printStackTrace();
				}
			});
		} catch (SftpException e) {
			e.printStackTrace();
		}
	}

	public static void gatherExpectedPayload(boolean localOrRemote) {
		String sourceDir = "campaignPayloads";
		String destDir = "runtimeTemplates";
		if (localOrRemote)
			readFilesfromLocal(sourceDir, destDir);
		else
			readFilesfromRemote(sourceDir, destDir);
	}

	private static void readFilesfromLocal(String srcFolder, String destFolder) {
		String localSrcDir = System.getProperty("user.home") + "/Documents/" + srcFolder;
		for (String fileName : new File(localSrcDir).list()) {
			String sourceDestination = localSrcDir + "/" + fileName;
			Path src = Paths.get(sourceDestination);
			Path dest = Paths.get(BaseSetup.resourcesDirectory + destFolder + "/" + fileName);
			try {
				Files.copy(src, dest);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
