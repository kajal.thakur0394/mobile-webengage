package com.webengage.mobile_automation.tests;

import com.webengage.mobile_automation.utils.Log;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import com.webengage.automation_core.apiCoreSetup.APIUtility;
import com.webengage.automation_core.apiCoreSetup.ConstantUtils;
import com.webengage.automation_core.apiCoreSetup.RuntimeUtility;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;
import com.webengage.mobile_automation.pages.EngageScreen;
import com.webengage.mobile_automation.utils.ApiOperationUtility;
import com.webengage.mobile_automation.utils.CustomAssertions;
import com.webengage.automation_core.apiCoreSetup.CustomLogFilter;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CommonTest {
	RuntimeUtility runtimeUtility = new RuntimeUtility();
	SetupUtility setupUtility = new SetupUtility();
	APIUtility apiUtility = new APIUtility();
	ApiOperationUtility apiOperations = new ApiOperationUtility();
	CustomLogFilter custLog = new CustomLogFilter();
	Response response;
	EngageScreen en;
	
	public void toggleCampaignStatus(boolean toggle, String apiName, String campaignId) {
		try {
			apiName = toggle ? "activate" + apiName : "deactivate" + apiName;
			APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, campaignId);
			runtimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
			setupUtility.setApiURI(apiName);
			apiUtility.putRequest(runtimeUtility.modifyAPIURL(setupUtility.getApiURI()));
			Log.info("Campaign has been toggled successfully");
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Campaign toggle error-> "+custLog.getLogResponse());
		}
	}

	public String fetchCampaignId(String api, String jsonPathforId, String campaignName) throws IOException {
		String id = null;
		setupUtility.setApiURI(api);
		String apiURI = setupUtility.getApiURI() + "?pageNo=1&pageSize=10";
		String queryParams;
		if (api.contains("Push") || api.contains("InApp")) {
			queryParams = "&sdks=2,3";
		} else if (api.contains("OnSite")) {
			queryParams = "&sdks=1";
		} else if (api.contains("ContentApp")) {
			queryParams = "&sdks=";
		} else {
			queryParams = "";
		}
		try {
			apiOperations.getMethod(apiURI + queryParams);
			response = apiUtility.getResponse();
			int count = Integer
					.parseInt(JsonPath.from(response.body().asString()).get("response.data.totalCount").toString());
			int pages = count / 10;
			pages = (count % 10) != 0 ? ++pages : pages;
			outer: for (int i = 1; i <= pages; i++) {
				apiURI = setupUtility.getApiURI() + "?pageNo=" + i + "&pageSize=10";
				apiOperations.getMethod(apiURI + queryParams);
				response = apiUtility.getResponse();
				int listSize = (i == pages && count % 10 == 0) ? 10 : (i == pages) ? (count % 10) : 10;
				for (int j = 0; j < listSize; j++) {
					id = JsonPath.from(response.body().asString())
							.get("response.data.contents[" + j + "]." + jsonPathforId).toString();
					String nameOrTitle = api.contains("Campaigns") ? "title"
							: api.contains("CustomTemplate") ? "templateName" : "name";
					String value = JsonPath.from(response.body().asString())
							.get("response.data.contents[" + j + "]." + nameOrTitle).toString();
					if (value.equalsIgnoreCase(campaignName)) {
						break outer;
					}
					if (api.contains("Campaigns")) {
						String category = JsonPath.from(response.body().asString())
								.get("response.data.contents[" + j + "].category").toString();
						if (category.equals("journey")) {
							continue;
						}
					}
				}
			}
		} catch (Exception e) {
			Log.error("Error in fetching campaign id->" + custLog.getLogResponse());
		}
		CustomAssertions.assertTrue("CampaignId is null",id != null);
		return id;
	}
	
	public void postTransaction(String id) throws IOException {
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, id);
		runtimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
		setupUtility.setApiURI("transactionalCampaign");
		runtimeUtility.modifyAPIURL(setupUtility.getApiURI());
		long startTime = System.currentTimeMillis();
		int dur = 60000;
		boolean flag = false;
		do {
			try {
				apiOperations.dynamic_post_method("transactionalCampaign", "PushCampaigns", "transactionalPushMobile");
				if (apiUtility.getResponse().getStatusCode() == 200
						|| apiUtility.getResponse().getStatusCode() == 201) {
					flag = true;
					break;
				}
			} catch (IOException | AssertionError e) {
				String response = custLog.getLogResponse();
				if (!response.contains("PUSH_NOTIFICATION") || (!response.contains("User not reachable")))
					break;
			}
		} while ((System.currentTimeMillis() - startTime) < dur);
		CustomAssertions.assertTrue("Error in transaction Api , status code->" + custLog.getLogResponse(), flag);
	}
	
	public JSONObject fetchCampaignTemplate(HashMap<Object,String> dataMap) throws ParseException {
		String expectedStr = dataMap.get("template");
		JSONParser parser = new JSONParser();
		JSONObject expected = (JSONObject) parser.parse(expectedStr);
		return (JSONObject) expected.get("Expected");
	}
}