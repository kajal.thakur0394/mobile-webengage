package com.webengage.mobile_automation.tests;

import com.webengage.mobile_automation.pages.EngageScreen;
import com.webengage.mobile_automation.utils.Log;

import org.testng.annotations.Test;

public class EngageTest {

	public EngageTest() {
		System.setProperty("noReset", "false");
	}

	public void initializeAndroid() {
		Log.info("Android Driver Setup");
		System.setProperty("set.Platform", "Android");
		System.setProperty("set.DeviceType", "Virtual");
	}

	public void initializeios() {
		Log.info("iOS Driver Setup");
		System.setProperty("set.Platform", "iOS");
		System.setProperty("set.DeviceType", "Virtual");
	}

	@Test
	public void test_android_engage() {
		initializeAndroid();
		new EngageScreen().androidSetup();
	}

	@Test
	public void test_ios_engage() {
		initializeios();
		new EngageScreen().iOSLogin();
	}

}