package com.webengage.mobile_automation.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;
import com.webengage.mobile_automation.dataProviders.CampaignPayloadDP;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.pages.EngageScreen;
import com.webengage.mobile_automation.pages.InAppCampaignView;
import com.webengage.mobile_automation.utils.ExtentReportUtil;
import com.webengage.mobile_automation.utils.Log;

public class InAppCampaignTest {

	InAppCampaignView inAppCamp;
	String campaignIdentifier;
	PreTest pre;
	CommonTest common = new CommonTest();
	SetupUtility setupUtility = new SetupUtility();
	String campaignId;
	EngageScreen en;
	String path = "";
	ExtentTest node;

	
	@BeforeTest
	public void initializeExtentReports() throws IOException {
		pre = new PreTest();
		String module = System.getProperty("set.Module");
		if (module.contains("InApp")) {
			ExtentReportUtil.initializeExtentReports();
			pre.initializeAndroid();
			ExtentReportUtil.createTest("InAppCampaign");
			node = ExtentReportUtil.extentTest;
		}
	}

	@Test(dataProvider = "InApp", dataProviderClass = CampaignPayloadDP.class)
	public void testInAppCamp(HashMap<Object, String> inAppMap)
			throws IOException, ParseException, InterruptedException {
		String campaignName = inAppMap.get("campaign_name");
		ExtentReportUtil.createNode(campaignName);
		Log.error("Verifying Campaign" + campaignName);
		// RESUME CAMPAIGN
		//campaignId = common.fetchCampaignId("fetchInAppCampaigns", "id", inAppMap.get("campaign_name"));
		//common.toggleCampaignStatus(true, "InApp", campaignId);
		//Log.error("CampaignId->" + campaignId + "has been resumed");
		//TimeUnit.SECONDS.sleep(150);
		en = new EngageScreen();
		en.iOSLogin();
		en.userLogin(inAppMap.get("user_id"));
		inAppCamp = new InAppCampaignView();
		//inAppCamp.exportScreenshot(false, null, campaignName + "UserLogin");
		inAppCamp.killAndLaunchApp();
		inAppCamp.waitForWebView();
		JSONObject expected = common.fetchCampaignTemplate(inAppMap);
		inAppCamp.validateContents(expected);
		//inAppCamp.exportScreenshot(false, null, campaignName + "InAppNotif");
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		//common.toggleCampaignStatus(false, "InApp", campaignId);
		if (result.getStatus() == ITestResult.FAILURE) {
			ExtentReportUtil.extentTest.fail(result.getName() + "Failed with Exceptions");
			//inAppCamp.exportScreenshot(false, null,"FailureInAppNotif");
		}
		DriverManager.driver.quit();
		DriverManager.driver = null;
		// Sibling node creation
		ExtentReportUtil.extentTest = node;
	}

	@AfterSuite
	public void generateExtentReports() throws IOException {
		ExtentReportUtil.generateExtentReports();
		//ExtentReportUtil.showExtentReportsonDesktop();
	}

}
