package com.webengage.mobile_automation.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;
import com.webengage.mobile_automation.dataProviders.CampaignPayloadDP;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.pages.EngageScreen;
import com.webengage.mobile_automation.pages.InlineAppPersonalisationPage;
import com.webengage.mobile_automation.utils.ExtentReportUtil;
import com.webengage.mobile_automation.utils.Log;

public class InlineAppPersonalisationTest {

	InlineAppPersonalisationPage inlineAppPage;
	String campaignIdentifier;
	PreTest pre;
	CommonTest common = new CommonTest();
	SetupUtility setupUtility = new SetupUtility();
	String campaignId;
	EngageScreen en;
	String path = "";
	ExtentTest node;

	
	@BeforeTest
	public void initializeExtentReports() throws IOException {
		pre = new PreTest();
		String module = System.getProperty("set.Module");
		if (module.contains("AppInLine")) {
			ExtentReportUtil.initializeExtentReports();
			pre.initializeAndroid();
			ExtentReportUtil.createTest("InLineCampaign");
			node = ExtentReportUtil.extentTest;
		}
	}

	@Test(dataProvider = "AppInLine", dataProviderClass = CampaignPayloadDP.class)
	public void testAppInlineCamp(HashMap<Object, String> inLineMap)
			throws IOException, ParseException, InterruptedException {
		String campaignName = inLineMap.get("campaign_name");
		ExtentReportUtil.createNode(campaignName);
		Log.error("Verifying Campaign" + campaignName);
		// RESUME CAMPAIGN
		campaignId = common.fetchCampaignId("fetchAppInLineCampaigns", "id", inLineMap.get("campaign_name"));
		common.toggleCampaignStatus(true, "AppInLine", campaignId);
		Log.error("CampaignId->" + campaignId + "has been resumed");
		TimeUnit.SECONDS.sleep(150);
		en = new EngageScreen();
		en.androidSetup();
		en.userLogin(inLineMap.get("user_id"));
		inlineAppPage = new InlineAppPersonalisationPage();
		inlineAppPage.exportScreenshot(false, null, campaignName + "UserLogin");
		//inlineAppPage.killAndLaunchApp();
		JSONObject expected = common.fetchCampaignTemplate(inLineMap);
		inlineAppPage.openLandingPage(expected);
		inlineAppPage.validateContents(expected);
		inlineAppPage.exportScreenshot(false, null, campaignName + "AppInLine");
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		common.toggleCampaignStatus(false, "AppInLine", campaignId);
		if (result.getStatus() == ITestResult.FAILURE) {
			ExtentReportUtil.extentTest.fail(result.getName() + "Failed with Exceptions");
			inlineAppPage.exportScreenshot(false, null,"FailureInLine");
		}
		DriverManager.driver.quit();
		DriverManager.driver = null;
		// Sibling node creation
		ExtentReportUtil.extentTest = node;
	}

	@AfterSuite
	public void generateExtentReports() throws IOException {
		ExtentReportUtil.generateExtentReports();
		//ExtentReportUtil.showExtentReportsonDesktop();
	}

}