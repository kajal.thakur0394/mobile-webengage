package com.webengage.mobile_automation.tests;

import com.webengage.mobile_automation.utils.Log;
import com.webengage.automation_core.remoteConnection.RemoteConnection;
import com.webengage.automation_core.apiCoreSetup.APIUtility;
import com.webengage.automation_core.apiCoreSetup.RuntimeUtility;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;
import com.webengage.automation_core.apiCoreSetup.RestAssuredBuilder;

import java.io.File;
import java.io.IOException;

public class PreTest {
	SetupUtility setupUtility = new SetupUtility();

	public PreTest() throws IOException {
		String environment = "Prod US";
		System.setProperty("set.Environment", environment);
		System.setProperty("set.Module", "InApp");
		System.setProperty("set.LicenseCode", "~10a5cb5c6");
		String namespace = "automation";
		namespace = namespace.replace("-", "").trim();
		System.setProperty("set.Namespace", namespace);
		String accountName = getAccountName(environment);
		try {
			setupUtility.loadCredentials(accountName);
		} catch (Exception e) {
			Log.warn("Credentials for account you have chosen are not known.");
		}
		initializeDataVariables();
	}

	public void initializeAndroid() throws IOException {
		Log.info("Android Driver Setup");
		System.setProperty("set.Platform", "iOS");
		System.setProperty("set.DeviceType", "Virtual");
		System.setProperty("noReset", "false");

	}

	public void initializeDataVariables() {
		APIUtility.initializeVariables();
		RuntimeUtility.initializeVariables();
		RestAssuredBuilder.initializeVariables();
	}
	
	public void flushDirectoryContents() {
		Log.info("Deleting stuff from Expected, Runtime Payloads, and CampaignStats");
		File fileLocations[] = { new File(SetupUtility.runtimePayloadsDirectory) };
		try {
			for (File file : fileLocations) {
				for (File dirFiles : file.listFiles()) {
					if (!dirFiles.getName().contains("gitkeep"))
						dirFiles.delete();
				}
			}
			RemoteConnection.readFilefromRemote("TestData", "testData.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getAccountName(String env) {
		String accountName = "";
		switch (env.toLowerCase()) {
		case "staging dev":
		case "staging":
			accountName = "QA Staging";
			break;
		case "prod beta":
		case "prod us":
		case "prod unl":
			accountName = "UIAutomation";
			break;
		case "prod ksa":
			accountName = "UIAutomationKSA";
			break;
		case "prod in":
			accountName = "UIAutomationIndia";
			break;
		case "qa":
			accountName = "UIAutomationQA";
			break;
		default:
			break;
		}

		return accountName;
	}
}