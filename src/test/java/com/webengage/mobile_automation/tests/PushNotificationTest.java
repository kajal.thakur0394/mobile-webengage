package com.webengage.mobile_automation.tests;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.webengage.automation_core.apiCoreSetup.SetupUtility;
import com.webengage.mobile_automation.dataProviders.CampaignPayloadDP;
import com.webengage.mobile_automation.driver.DriverManager;
import com.webengage.mobile_automation.pages.EngageScreen;
import com.webengage.mobile_automation.pages.PushNotificationsPanel;
import com.webengage.mobile_automation.utils.ExtentReportUtil;
import com.webengage.mobile_automation.utils.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class PushNotificationTest {

	String campaignIdentifier;
	PreTest pre;
	CommonTest common = new CommonTest();
	SetupUtility setupUtility = new SetupUtility();
	String campaignId;
	EngageScreen en;
	String path = "";
	ExtentTest node;
	PushNotificationsPanel pushNotif;

	@BeforeTest
	public void initializeExtentReports() throws IOException {
		pre = new PreTest();
		String module = System.getProperty("set.Module");
		if (module.contains("Push")) {
			ExtentReportUtil.initializeExtentReports();
			pre.initializeAndroid();
			ExtentReportUtil.createTest("PushCampaign");
			node = ExtentReportUtil.extentTest;
		}
	}

	@Test(dataProvider = "Push", dataProviderClass = CampaignPayloadDP.class)
	public void testPushCamp(HashMap<Object, String> pushMap) throws IOException, ParseException, InterruptedException {
		String campaignName = pushMap.get("campaign_name");
		ExtentReportUtil.createNode(campaignName);
		en = new EngageScreen();
		en.androidSetup();
		en.userLogin(pushMap.get("user_id"));
		pushNotif = new PushNotificationsPanel();
		Log.error("Verifying Campaign" + campaignName);
		campaignId = common.fetchCampaignId("fetchPushCampaigns", "id", pushMap.get("campaign_name"));
		common.postTransaction(campaignId);
		pushNotif.openNotificationPanel();
		JSONObject expected = common.fetchCampaignTemplate(pushMap);
		pushNotif.filterAppNotifications(expected);
		pushNotif.exportScreenshot(false, null, campaignName + "PushNotif");
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			ExtentReportUtil.extentTest.fail(result.getName() + "Failed with Exceptions");
			((AndroidDriver) DriverManager.driver).pressKey(new KeyEvent(AndroidKey.HOME));
			pushNotif.exportScreenshot(false, null, "FailureInAppNotif");
		}
		DriverManager.driver.quit();
		DriverManager.driver = null;
		// Sibling node creation
		ExtentReportUtil.extentTest = node;
	}

	@AfterSuite
	public void generateExtentReports() throws IOException {
		ExtentReportUtil.generateExtentReports();
		ExtentReportUtil.showExtentReportsonDesktop();
	}

}
